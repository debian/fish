Source: fish
Section: shells
Priority: optional
Maintainer: Mo Zhou <lumin@debian.org>
Build-Depends: debhelper-compat (= 13),
               cargo,
               cmake,
               gettext,
               libpcre2-dev,
               locales-all <!nocheck>,
               ninja-build,
               procps <!nocheck>,
               python3 <!nocheck>,
               python3-sphinx,
               python3-pexpect <!nocheck>,
               rustc,
               librust-bitflags-dev,
               librust-cc-dev,
               librust-errno-dev,
               librust-lazy-static-dev,
               librust-libc-dev (>= 0.2.169~),
               librust-lru-dev,
               librust-nix-dev,
               librust-num-traits-dev,
               librust-once-cell-dev,
               librust-portable-atomic-dev,
               librust-rand-dev,
               librust-rsconf-dev,
               librust-rust-embed-dev,
               librust-serial-test-dev,
               librust-tempfile-dev,
               librust-terminfo-dev (>= 0.9.0~),
               librust-widestring-dev,
               ncurses-term,
               tmux <!nocheck>,
               git <!nocheck>,
Standards-Version: 4.7.0
Homepage: http://fishshell.com/
Vcs-Browser: https://salsa.debian.org/debian/fish
Vcs-Git: https://salsa.debian.org/debian/fish.git
Rules-Requires-Root: no

Package: fish
Architecture: any
Depends: fish-common (= ${source:Version}),
         bsdextrautils,
         groff-base,
         man-db,
         procps,
         python3,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: xsel, lynx | www-browser,
Suggests: doc-base
Description: friendly interactive shell
 Fish is a shell geared towards interactive use.  Its features are focused on
 user friendliness and discoverability.  The language syntax is simple but
 incompatible with other shell languages.

Package: fish-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: fish, python3:any
Description: friendly interactive shell (architecture-independent files)
 Fish is a shell geared towards interactive use.  Its features are focused on
 user friendliness and discoverability.  The language syntax is simple but
 incompatible with other shell languages.
 .
 This package contains the common fish files shared by all architectures.
